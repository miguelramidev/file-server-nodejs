const dotenv = require('dotenv');
const path = require('path');

dotenv.config();

const config = {
    static: path.join(__dirname, 'static'),
    port: parseInt(process.env.PORT),
    host: process.env.HOST.toString()
}

module.exports = config;