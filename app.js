const express = require('express');
const cors = require('cors');
const fileUpload = require('express-fileupload');
const config = require('./config');
const functions = require('./functions');
const { Router } = require('express');

const app = express()
const routes = Router();


app.use(express.json({limit: '200mb'}));
app.use(express.urlencoded({extended: false, limit: '200mb'}));
app.use(express.text({limit: '200mb'}))
app.use(cors({allowedHeaders: '*'}));
app.use(fileUpload({createParentPath: true}));

app.use('/static', express.static(config.static));

routes.post('/add', functions.add);
routes.post('/remove', functions.remove);

app.use('/', routes);

module.exports = app;