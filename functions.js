const fs = require('fs-extra');
const {uid} = require('uid');
const path = require('path');
const config = require('./config');
const base64 = require('base-64');
const {Blob} = require('node:buffer')

/**
 * @description Obtiene el tipo del file
 * @param {string} mimeType MimeType del file
 * @returns {string} Tipo del file Ej: jpg, png
 */
function getFileType(mimeType) {
    if (mimeType.split('/').pop().includes('+')) {
        return mimeType.split('/').pop().split('+')[0]
    }
    return mimeType.split('/').pop();
}


/**
 * @description Elimina un archivo
 * @param {string} filePath Path del archivo que se va a eliminar
 */
async function deleteFile(filePath) {
    filePath = filePath.replace(`${config.host}/static/`, '');
    const filePathArray = filePath.split('/');
    await fs.remove(path.join(__dirname, 'static', ...filePathArray));
}

/**
 * @description Elimina una imagen
 * @param {import("express").Request} req 
 * @param {import("express").Response} res 
 */
const remove = async (req, res) => {
    try {
        const {ip, hostname} = req;
        console.log('connect from:', ip, hostname);

        /**
         * @type {Array<string>}
         */
        const files = req.body.files;

        for (const filePath of files) {
            await deleteFile(filePath)
        }

        res.status(200).send({success: true})
    } catch (error) {
        const title = 'REMOVE FILE ERROR'
        console.log(title, error);
        res.status(500).send({title, error});
    }
}

/**
 * @description Añade una imagen
 * @param {import("express").Request} req 
 * @param {import("express").Response} res 
 */
const add = (req, res) => {
    try {
        const {ip, hostname} = req;
        console.log('connect from:', ip, hostname);

        /**
         * @type {string}
         */
        const dir = req.body.dir;

        let files = req.files.files;

        if (!dir && typeof dir !== 'string') {
            res.status(400).send({error: 'dir is required and string'});
            return;
        }

        if (!files) {
            res.status(400).send({error: 'images is required and not length 0'});
            return;
        }

        if (!Array.isArray(files)) {
            files = [files]
        }

        const staticLocation = path.join(__dirname, 'static');

        if (!fs.existsSync(staticLocation)) {
            fs.mkdirSync(staticLocation)
        }

        const dirLocation = path.join(__dirname, 'static', dir);

        if (!fs.existsSync(dirLocation)) {
            fs.mkdirSync(dirLocation)
        }
        
        const filePaths = [];

        for (const file of files) {
            const fileName = `${uid(32)}.${getFileType(file.mimetype)}`;
            const filePath = `${config.host}/static/${dir}/${fileName}`;
            file.mv(path.join(__dirname, 'static', dir, fileName))
            filePaths.push(filePath);
        }

        res.status(200).json(filePaths);
    } catch (error) {
        const title = 'ADD FILE ERROR';
        console.log(title, error);
        res.status(500).send({title, error});
    }
}



module.exports = {
    add,
    remove
}